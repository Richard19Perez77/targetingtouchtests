package android.target.test;

import android.test.ActivityInstrumentationTestCase2;
import android.target.*;

public class TouchTests extends ActivityInstrumentationTestCase2<MainActivity> {
	private MainActivity mainActivity;

	public TouchTests() {
		super(MainActivity.class);
	}

	public TouchTests(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();
	}
	
	public void testActivityNotNull(){
		assertNotNull(mainActivity);
	}

	public void testStartStatePaused() {
		int mode = mainActivity.drawingSurface.targetThread.mMode;
		assertEquals(mode, TargetThread.STATE_PAUSE);
	}
}
